﻿<%@ Page Title="User Administration" Language="C#" MasterPageFile="~/MasterPages/3Column.Master" AutoEventWireup="true" CodeBehind="UserAdministration.aspx.cs" Inherits="SimplePortal.AdminArea.UserAdministration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Njumbotron" runat="server">
    User Administration 2
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="NLeftHead" runat="server">Create User
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="NLeftParag" runat="server">
    <p>
        All required attribures in Account colection </p>
        
            <table ID="TableAddAccount">
                <tr class="TableTitles">
                    <td>Attribute</td>
                    <td class="auto-style2">Entry Value</td>
                </tr>      
                <tr>
                    <td>Phone #:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Password:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox2" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Username:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox3" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>First Name:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox4" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Last Name:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox5" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Home Airport Id:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox6" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Privilege:</td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox7" runat="server" Visible="False"></asp:TextBox></td>
                </tr>         

            </table>
 
        </asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NMidHead" runat="server">Maintain User
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NMidParag" runat="server">
    <p>
        edit user details<br />
    </p>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NRightHead" runat="server">
        View and Maintain Users  
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="NRightParag" runat="server">
    <p>
        Ideally this would be a &quot;getAllAccounts&quot; function that populates a Grid View that supports edit rows function</p>
    <p>
        [Grid VIew Here]<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="odsUsers">
            <Columns>
                <asp:CheckBoxField DataField="Success" HeaderText="Success" SortExpression="Success" />
                <asp:BoundField DataField="Error" HeaderText="Error" SortExpression="Error" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="odsUsers" runat="server" DeleteMethod="Delete" InsertMethod="Post" SelectMethod="Post" TypeName="SimplePortal.Helpers.RestClientHelper" UpdateMethod="Post">
            <DeleteParameters>
                <asp:Parameter Name="resource" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="function" Type="String" />
                <asp:Parameter Name="request" Type="Object" />
            </InsertParameters>
            <SelectParameters>
                <asp:Parameter DefaultValue="getAccounts" Name="function" Type="String" />
                <asp:Parameter DefaultValue="" Name="request" Type="Object" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="function" Type="String" />
                <asp:Parameter Name="request" Type="Object" />
            </UpdateParameters>
        </asp:ObjectDataSource>
    </p>
    <p>
        &nbsp;</p>
    <p>
        If not, then we&#39;ll use drop down list of all users to select one to edit.</p>
    <p>
        View and Edit selected user:</p>
    <p>
        call getAccount function:</p>
    <p>
        <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        [Edit info in table similar to MOMaintenance page.]</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>
