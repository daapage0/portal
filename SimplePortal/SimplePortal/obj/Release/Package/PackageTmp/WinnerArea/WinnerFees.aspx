﻿<%@ Page Title="Winner" Language="C#" MasterPageFile="~/MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="WinnerFees.aspx.cs" Inherits="SimplePortal.WinnerArea.WinnerFees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle" runat="server">
        Maintain Fees
    for Winner 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="col-md-3 column1 ContentContainer">
<h3>Fixed Based Operations</h3>
        <div class = "SmallFont">
        <table ID="TableEditMisc2">    
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td >Amount</td>
    </tr> 
    <tr>
        <td>Security Fee:</td>
        <td class="auto-style1">
            <asp:Label ID="lblSecurityFee" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtSecurityFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" BackColor="#E2E2E2" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Handling Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblHandlingFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtHandlingFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Parking Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblParkingFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtParkingFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Ramp Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblRampFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtRampFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Fuel Surcharge Rate:</td>
        <td class="auto-style1"><asp:Label ID="lblFuelSurchargeRate" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFuelSurchargeRate" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>
    </table>
</div >
    <div >
    <p>
        <asp:Button ID="btnEditFBO" runat="server" OnClick="btnEditFBO_Click" Text="Edit FBO" CssClass="button" />
        <asp:Button ID="btnConfirmFBO" class="button" runat="server" OnClick="btnConfirmFBO_Click" Text="Confirm FBO" Visible="False" Width="127px" CssClass="button" />
<%--        <button id="btnConfirmFBO" onclick="btnConfirmFBO_Click">Click me to COnfirm FBO</button>--%>
    </p>
    </div>
</div>

<div class ="col-md-3 column2 ContentContainer">
<h3>Flight Operations</h3>
        <div class = "SmallFont">
    <table ID="TableEditMisc1">
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td >Amount</td>
    </tr>      
    <tr>
        <td>Variable Segment Fee:</td>
        <td class="auto-style1">
            <asp:Label ID="lblVariableSegmentFee" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtVariableSegmentFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Fixed Segment Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFixedSegmentFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFixedSegmentFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td colspan="2">Fixed or Variable Segment Fee?<br />
            <asp:RadioButtonList CssClass ="inline-rb" ID="rblSegmentFee" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" BorderStyle="None">
            <asp:ListItem >Fixed</asp:ListItem>
            <asp:ListItem>Variable</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>    
    <tr>
        <td>Aircraft Hourly Rate:</td>
        <td class="auto-style1"><asp:Label ID="lblAircraftHourlyRate" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtAircraftHourlyRate" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Daily Crew Cost:</td>
        <td class="auto-style1"><asp:Label ID="lblDailyCrewCost" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtDailyCrewCost" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    </table>
</div>
    <div>
        <p>
        <asp:Button ID="btnEditOps" runat="server" OnClick="btnEditOps_Click" Text="Edit Ops" CssClass="button" />
        <asp:Button ID="btnConfirmOps" runat="server" OnClick="btnConfirmOps_Click" Text="Confirm Ops" Visible="False" Width="127px" CssClass="button" />
        </p>
</div>
</div>

<div class ="col-md-3 column3 ContentContainer">
<h3>Airport</h3>
    <div class = "SmallFont">
    <table ID="TableEditMisc0">
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td class="auto-style1">Amount</td>
    </tr>      
        <tr>
        <td>Airport Concession Rate:</td>
        <td class="auto-style1"><asp:Label ID="lblAirportConcession" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtAirportConcession" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
        </tr>
    <tr>
        <td>Taxi Time (Minutes):</td>
        <td class="auto-style1">
            <asp:Label ID="lblTaxiMinutes" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtTaxiMinutes" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Taxi Time (Offset Rate %):</td>
        <td class="auto-style1"><asp:Label ID="lblTaxiOffset" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtTaxiOffset" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>

    </tr>    
    <tr>
        <td colspan="2">Taxi Time in Minutes or Offset % 
            <asp:RadioButtonList CssClass ="inline-rb" ID="rblTaxi" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" BorderStyle="None" Width="289px">
            <asp:ListItem >Average Minutes</asp:ListItem>
            <asp:ListItem>Offset %</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>    
    <tr>
        <td>2nd Landing Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFee2ndLanding" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFee2ndLanding" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Fixed Landing Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFixedLandingFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFixedLandingFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Variable Landing Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblVariableLandingFee" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtVariableLandingFee" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td colspan="2">Landing Fee Fixed or Variable?
            <asp:RadioButtonList CssClass ="inline-rb" ID="rblLandingFee" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" BorderStyle="None" Height="16px">
            <asp:ListItem >Fixed</asp:ListItem>
            <asp:ListItem>Variable</asp:ListItem>
            </asp:RadioButtonList>
    </tr>    
    </table>
</div>
    <p>
        <asp:Button ID="btnEditAirport" runat="server" OnClick="btnEditAirport_Click" Text="Edit Airport" CssClass="button" />
        <asp:Button ID="btnConfirmAirport" runat="server" OnClick="btnConfirmAirport_Click" Text="Confirm Airport" Visible="False" Width="127px" CssClass="button" />
    </p>
</div>

<div class = "col-md-3 column4 ContentContainer">
<h3>Miscellaneous</h3>
    <div class = "SmallFont" title =" test definition area">
    <table ID="TableEditMisc">
    <tr class="TableTitles">
        <td>Fee Name</td>
        <td class="auto-style1">Amount</td>
    </tr>      
    <tr>
        <td title="Explain Callout Fee.">FeeCallout:</td>
        <td class="auto-style1">
            <asp:Label ID="lblFeeCallout" runat="server" Text="Label"></asp:Label>
            <asp:TextBox ID="txtFeeCallout" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Hangar Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeHanger" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeHanger" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Lav Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeLav" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeLav" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Ground Power Unit Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeGPU" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeGPU" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Overnight Fee:</td>
        <td class="auto-style1"><asp:Label ID="lblFeeOvernight" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtFeeOvernight" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td>Nightly Crew Expense:</td>
        <td class="auto-style1"><asp:Label ID="lblExpensesNightlyCrew" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="txtExpensesNightlyCrew" runat="server" BorderStyle="None" BorderWidth="0px" Height="16px" Visible="False" Width="96px" ></asp:TextBox>
        </td>
    </tr>    
    </table>
</div>
    <p>
        <asp:Button ID="btnEditMisc" runat="server" OnClick="btnEditMisc_Click" Text="Edit Misc" CssClass="button" />
        <asp:Button ID="btnConfirmMisc" runat="server" OnClick="btnConfirmMisc_Click" Text="Confirm Misc" Visible="False" Width="127px" CssClass="button" />
    </p>
</div>


</asp:Content>
