﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SimplePortal.Results
{
    #region " Namespace Declarations "

    public class Trip
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("departureLocation")]
        public string DepartureLocation { get; set; }

        [JsonProperty("arrivalLocation")]
        public string ArrivalLocation { get; set; }

        [JsonProperty("departureDate")]
        public DateTime DepartureDate { get; set; }

        [JsonProperty("arrivalDate")]
        public string ArrivalDate { get; set; }

        [JsonProperty("flightTime")]
        public DateTime FlightTime { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("cost")]
        public int Cost { get; set; }

        [JsonProperty("openSeats")]
        public int OpenSeats { get; set; }

        [JsonProperty("fboName")]
        public string FboName { get; set; }

        [JsonProperty("fboId")]
        public string FboId { get; set; }

        [JsonProperty("fboAgreement")]
        public string FboAgreement { get; set; }

        [JsonProperty("flytzeCreated")]
        public Boolean FlytzeCreated { get; set; }

        [JsonProperty("planeId")]
        public string PlaneId { get; set; }
    }

    public class OperationResult
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }
    }

    #endregion

    /// <summary>
    /// trips results
    /// </summary>
    public class Trips : OperationResult
    {

        [JsonProperty("departureDate")]
        public List<Trip> departureDate { get; set; }

        [JsonProperty("departures")]
        public List<Trip> Departures { get; set; }
    }
}