﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using System.Resources;
using RestSharp;
using SimplePortal.Objects;

namespace SimplePortal.Helpers
{
    public class RestClientHelper
    {
        /// <summary>
        /// rest client helper
        /// </summary>
        /// 
        #region  " Private Methods "

        private static string FlytzeServiceUrl = "https://openexchangerates.org";
        private static string FlytzeServiceUrl2 = "http://flytzedev-flytzedev1deployment.azurewebsites.net/thewall";
        //ConfigurationManager.AppSettings["Flytze.Service.Url"];

        #endregion

        #region " Public Methods "          
        ///        /// <summary>
        /// post to a resource
        /// </summary>
        //public static WallResponse<T1> Post<T1>( string function, object request)
        public static WallResponse<T1> Post<T1>(string function, object request)
        {
            var client = new RestClient(FlytzeServiceUrl2);
            var sameSalt = Security.Salt(20);

            // DO: build the wall request
            var wallRequest = new WallRequest();
            wallRequest.function = function;
            wallRequest.functionParams = request;
            wallRequest.pepper = "sdeODQ0T25oEBOvZW72xOXrOVXZOkz1HWNt9Now4DJ0EEdWXeA0ctGXbqqlxKuJp3H7MSyBK6BAt1DKipXNVMmSx0H"; // TODO [dpage] : get pepper
            //saltUser -> encode-> createHash = saltSecure
            wallRequest.saltSecure = Security.createHash(Security.encodeUTF8(Security.saltUser(sameSalt)));
            //"5aa0a56fed36966e625350259fe2c224fa17d26820dfaae343f3b9c8";
            wallRequest.sentTime = DateTime.UtcNow.AddMinutes(-35).ToString("yyyy-MM-ddThh:mm:ssZ");
            wallRequest.token = "FawE5e1sE2P49aE1ds";
            wallRequest.username = HttpContext.Current.User.Identity.Name.ToString();
            //"brycesublette@gmail.com";
            var toEncode = JsonConvert.SerializeObject(wallRequest);
            var toHash = Security.encodeUTF8(toEncode);
            var hashedRequest = Security.createHash(toHash);

            //DO: build the secure wall request
            var wallSecure = new WallRequestSecure();
            wallSecure.function = wallRequest.function;
            wallSecure.hash = hashedRequest;
            //Security.createHashRequest(wallRequest, hash);
            wallSecure.functionParams = wallRequest.functionParams;
            wallSecure.salt = sameSalt;
            wallSecure.sentTime = wallRequest.sentTime;
            wallSecure.username = wallRequest.username;
            //need to serialize here 
            var requestSecureToSend = JsonConvert.SerializeObject(wallSecure);

            WallResponse<T1> wallResponse = new WallResponse<T1>();
            //requestSecureToSend looks legit, issue is likely below...
            //var req = new RestRequest(Method.POST);   gets noTemplate.FBOs
            //var req = new RestRequest(requestSecureToSend);   not JSON in request
            //var req = new RestRequest(requestSecureToSend, Method.POST);   not JSON in request
            var req = new RestRequest(Method.POST);
            req.RequestFormat = DataFormat.Json;
            req.AddHeader("Content-Type", "application/json");
            req.AddBody(wallSecure);
            var response = client.Execute(req);
            return JsonConvert.DeserializeObject<WallResponse<T1>>(response.Content);
        }


        public static T1 Delete<T1>(string resource)
        {
            var client = new RestClient(FlytzeServiceUrl);
            var req = new RestRequest(resource, Method.DELETE);
            var response = client.Execute(req);
            return JsonConvert.DeserializeObject<T1>(response.Content);
        }
        /// <summary>
        /// read to a resource
        /// </summary>
        public static T1 Get<T1>(string resource)
        {
            var client = new RestClient(FlytzeServiceUrl);
            var req = new RestRequest(resource, Method.GET);
            var response = client.Execute(req);
            return JsonConvert.DeserializeObject<T1>(response.Content);
        }
        /// <summary>
        /// post to a resource
        /// </summary>
        public static T1 Put<T1>(string resource, object request)
        {
            var client = new RestClient(FlytzeServiceUrl);
            var req = new RestRequest(resource, Method.PUT);
            req.AddObject(request);
            var response = client.Execute(req);
            return JsonConvert.DeserializeObject<T1>(response.Content);
        }
        #endregion
    }
}