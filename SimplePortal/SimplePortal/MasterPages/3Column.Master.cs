﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity.Owin;
using Owin;

namespace SimplePortal.MasterPages
{
    public partial class _3Column : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Master.FindControl("NavView").FindControl("DeveloperWallTest").Visible = false;
            this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = false;
            this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = false;

            //this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = false;
            //this.Page.Master.FindControl("NavView").FindControl("SecondFBOFees").Visible = false;

            //var userName = HttpContext.Current.User.Identity.Name.ToString();
            //switch (userName)
            //{
            //    case "developer@flytze.com":
            //        this.Page.Master.FindControl("NavView").FindControl("DeveloperWallTest").Visible = true;
            //        this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = true;
            //        this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = true;
            //        this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = true;
            //        this.Page.Master.FindControl("NavView").FindControl("SecondFBOFees").Visible = true;
            //        break;
            //    case "admin@flytze.com":
            //        this.Page.Master.FindControl("NavView").FindControl("AdminMaintainMO").Visible = true;
            //        this.Page.Master.FindControl("NavView").FindControl("AdminUserAdmin").Visible = true;
            //        this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = true;
            //        this.Page.Master.FindControl("NavView").FindControl("SecondFBOFees").Visible = true;
            //        break;
            //    case "winner@flytze.com":
            //        this.Page.Master.FindControl("NavView").FindControl("WinnerFees").Visible = true;
            //        break;
            //    case "secondfbo@flytze.com":
            //        this.Page.Master.FindControl("NavView").FindControl("SecondFBOFees").Visible = true;
            //        break;
            //}
        }

        protected void btnFlytzeHome_Click1(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void LogOut_LoggingOut(Object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
    }
}