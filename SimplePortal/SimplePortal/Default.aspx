﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/MasterPages/Portal.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SimplePortal.Default" %>
<%--                   <div class="Nav">
                    <ul class="pull-right" runat="server">
                        <li><a runat="server" id="WallTest" href="../ExchangeRatesPage.aspx">Wall Test2</a></li>
                        <li><a runat="server" href="../FBOArea/OperatorFeeMaint_4Col.aspx">Manage Costs2</a></li>
                        <li><a runat="server" href="../AdminArea/MOMaintenance.aspx">Manage Factors2</a></li>
                    </ul>
                </div>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">Flytze Home Page</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="column1 col-md-4">
<h3>The Mission </h3>
    <p>
    Our team is working on time travel and teleportation, but until we get the kinks fixed, using Flytze is the most convenient and glamorous method of travel available today.&nbsp;
</p>
<p>
    If you&#39;re not new to private aviation, <br />Flytze is still on your schedule, suited to your preferences, but has the opportunity to be less impactful on your bottom line should you choose to crowdshare.</p>
<p>
    If you&#39;re a chartered flight rookie, <br />you now have the opportunity to join an exhisting itenerary and experience the luxury without the expense when you join a crowdshared flight.</p>
</div>

<div class="column2 col-md-4">
<h3>The Flytze Advantage</h3>
    <h4>Take Control. Take Adventures. Take Flytze. </h4>
        <p>
        Unforgiving airport security.<br />
        Inconvenient departure times.<br />
        Claustrophobia-inducing middle seats.<br />
        Boarding processes akin to cattle hearding.<br />
        Parking lot style traffic outside airport terminals.<br />
        Stale pretzals and half of an ounce of warm water.<br />
        Near worthless mobile apps that refuse to check you in.
    </p>
    <p>Commercial Aviation.</p>
    <p>
        Like an old pair of shoes that hurt your posture and degrade your health but you&#39;ve had them so long you don&#39;t think anything is wrong with them (like a bad habit), shedding your unnecessary addiction to commercial travel will do wonders for your wellbeing.&nbsp; After your first Flytze experience the old way of getting around will seem like a faint and distant nightmare.</p>
    <p>
        The digital age has ushered in levels of availability that were deemed impossible before.&nbsp; We&#39;ve never been more connected, services never as accessable.&nbsp; More available, Accessability to anything imaginable through a few swift taps of our fingers.&nbsp; Control is in your hands, not the hands of a boardroom focused on their profits rather than your pleasure.&nbsp; Yes, things will be different going forward.&nbsp; You can let your TSA Precheck expire.&nbsp; You don&#39;t need to pay for that club lounge membership.&nbsp; Even your suitecase size restrictions are no more.</p>
    <p>
        For those easy &quot;we&#39;re suffering through this together&quot; ice breakers with coworkers you may not know so well, you can still complain about the weather...</p>
</div>


<div class="column3 col-md-4">
    <h3>The Version</h3>
    <p>This deployment of the Administration Portal shows the view from the FBO perspective.  Other tabs will be available for Flytze admins soon, but not visible to FBO users.</p>
<%--<h3>Test Users</h3>
        <p>
    developer@flytze.com = developer account, sees work in progress pages.</p>
<p>
    admin@flytze.com = admin user, full authorization.</p>
<p>
    winner@flytze.com = test user for Winner Aviation users.</p>
<p>
    secondfbo@flytze.com = test user for other potential third parties.</p>
<p>
    *all passwords are: Pa$$word1</p>
    <p>--%>
</div>

</asp:Content>
