﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Helpers;
using SimplePortal.Objects;

namespace SimplePortal.AdminArea
{
    public partial class UserAdministration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //1. User performs action (ex. input user info, click button to submit)

            //2. Get saltSecure for every action
            //saltUser -> encode-> createHash = saltSecure
            var salt = Security.Salt(20);
            var newSaltUser = Security.saltUser(salt);
            var saltUserEncoded = Security.encodeUTF8(newSaltUser);
            Security.createHash(saltUserEncoded);

            //3. Build Json request
            //**assumptions: funciton = registerUser
            WallRequest registerUser = new WallRequest();


            //4. Encode Json request

            //5. hash Json request

            //6. Build Json requestSecure

            //7. POST Json requestSecure to Wall


        }
    }
}