﻿<%@ Page Title="Maintain MO" Language="C#" MasterPageFile="../MasterPages/3Column.Master" AutoEventWireup="true" CodeBehind="MOMaintenance.aspx.cs" Inherits="SimplePortal.AdminArea.MOMaintenance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 38px;
        }
        .auto-style2 {
            width: 102px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Njumbotron" runat="server">
    Manage Monitization Optimization Figures
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="NLeftHead" runat="server">
    Update Factor on All Planes
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="NLeftParag" runat="server">
    <div class = "LargeFont">
        Editing these fields updates the upcharge factor for the Pax position for all planes and makes all planes consistent
    </div>
    <div class = "SmallFont">
    <p>
        <asp:Button ID="btnEditAll" runat="server" OnClick="btnEditAll_Click" Text="Edit All Planes" />
        <asp:Button ID="btnConfirmAll" runat="server" Text="Confirm Edits" OnClick="btnConfirmAll_Click" Visible="False" />
    </p>
    <p>
        Seat 2:
        <asp:Label ID="Label2a" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox2a" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        Seat 3:
        <asp:Label ID="Label3a" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox3a" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        Seat 4:
        <asp:Label ID="Label4a" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox4a" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        Seat 5:
        <asp:Label ID="Label5a" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox5a" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        Seat 6:
        <asp:Label ID="Label6a" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox6a" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        Seat 7:
        <asp:Label ID="Label7a" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox7a" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        Seat 8:
        <asp:Label ID="Label8a" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox8a" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        
        
    </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        
            <table ID="TableEditAllPlanes">
                <tr class="TableTitles">
                    <td>PAX</td>
                    <td class="auto-style1">Current</td>
                    <td class="auto-style2">Change To</td>
                </tr>      
                <tr>
                    <td>Seat 1:</td>
                    <td class="auto-style1"><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 2:</td>
                    <td class="auto-style1"><asp:Label ID="Label2" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox2" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 3:</td>
                    <td class="auto-style1"><asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox3" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 4:</td>
                    <td class="auto-style1"><asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox4" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 5:</td>
                    <td class="auto-style1"><asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox5" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 6:</td>
                    <td class="auto-style1"><asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox6" runat="server" Visible="False"></asp:TextBox></td>
                </tr>                
                <tr>
                    <td>Seat 7:</td>
                    <td class="auto-style1"><asp:Label ID="Label7" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox7" runat="server" Visible="False"></asp:TextBox></td>
                </tr>         
                <tr>
                    <td>Seat 8:</td>
                    <td class="auto-style1"><asp:Label ID="Label8" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox8" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Seat 9:</td>
                    <td class="auto-style1"><asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox9" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Seat 10:</td>
                    <td class="auto-style1"><asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style2"><asp:TextBox ID="TextBox10" runat="server" Visible="False"></asp:TextBox></td>
                </tr>
            </table>
 
        </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="NMidHead" runat="server">

    Update Factor on Specific Plane 

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="NMidParag" runat="server">
    <div class = "LargeFont">
        Editing these fields update the upcharge factor for the Pax position on selected plane</div>
    <div class =" SmallFont">
    <p>
        Select FBO owner of plane:
        <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        </asp:DropDownList>
    </p>
    
        <div id ="CheckitDiv">
    </div>
    <p>
        &nbsp;</p>
    <p>
        Select Plane to Update:
        <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <asp:DropDownList ID="DropDownList2" runat="server">
        </asp:DropDownList>
    </p>
    <p>
        <asp:CheckBoxList ID="CheckBoxList1" runat="server" Width="115px" TextAlign="right" RepeatDirection="Horizontal">
            <asp:ListItem>Plane 1</asp:ListItem>
            <asp:ListItem>Plane 2</asp:ListItem>
            <asp:ListItem>Plane 3</asp:ListItem>
        </asp:CheckBoxList>
    </p>
    <p>
        Seat 2:
        <asp:Label ID="Label24" runat="server" Text="Label" CssClass="glyphicon-tower"></asp:Label>
        <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 3:
        <asp:Label ID="Label25" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 4:
        <asp:Label ID="Label26" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 5:
        <asp:Label ID="Label27" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 6:
        <asp:Label ID="Label28" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 7:
        <asp:Label ID="Label29" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 8:
        <asp:Label ID="Label30" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 9:
        <asp:Label ID="Label31" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>
    </p>
    <p>
        Seat 10:
        <asp:Label ID="Label32" runat="server" Text="Label"></asp:Label>
        <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox>
    </p>
        </div>
    <p>
        <asp:Button ID="btnConfirmPlanes" runat="server" OnClick="Button1_Click" Text="Confirm Edits" Width="124px" />
    </p>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="NRightHead" runat="server">
    <h3>Output from Changes</h3>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="NRightParag" runat="server">
    <div class = "LargeFont">
        The cumulative results for each seat.</div>
    <div class ="SmallFont">
    <p>
        PAX 1:&nbsp;
        <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 2:&nbsp;
        <asp:Label ID="Label14" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 3:&nbsp;
        <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 4:&nbsp;
        <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 5:&nbsp;
        <asp:Label ID="Label17" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 6:&nbsp;
        <asp:Label ID="Label18" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 7:&nbsp;
        <asp:Label ID="Label19" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 8:&nbsp;
        <asp:Label ID="Label20" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 9:&nbsp;
        <asp:Label ID="Label21" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        PAX 10:&nbsp;
        <asp:Label ID="Label22" runat="server" Text="Label"></asp:Label>
    </p>
        </div>
</asp:Content>
