﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SimplePortal.Objects;

namespace SimplePortal.AdminArea
{
    public partial class MOMaintenance : System.Web.UI.Page
    {
        // Instantiate sample FBO for plane ownership
        FBO Winner = new FBO();

        

        //Winner().Name = "Winner";
        PAXFactors MiniBoeing = new PAXFactors("MiniBoeing");

        protected void Page_Load(object sender, EventArgs e)
        {
            Winner.Name = "Winner";
            CheckBoxList1.Visible = true;
            
            //only show lables if all planes have the same PAX-Seat Factors
            //else show nothing and pop up textboxes if they want to edit all planes
            var P1 = MiniBoeing.PAX1 = .05;
            var P2 = MiniBoeing.PAX2 = .05;
            var P3 = MiniBoeing.PAX3 = .03;
            var P4 = MiniBoeing.PAX4 = .03;
            var P5 = MiniBoeing.PAX5 = .025;
            var P6 = MiniBoeing.PAX6 = .025;
            var P7 = MiniBoeing.PAX7 = .02;
            var P8 = MiniBoeing.PAX8 = .02;
            var P9 = MiniBoeing.PAX9 = .01;
            var P10 = MiniBoeing.PAX10 = .01;

            Label1.Text = P1.ToString("P");
            Label2.Text = P2.ToString("P");
            Label3.Text = P3.ToString("P");
            Label4.Text = P4.ToString("P");
            Label5.Text = P5.ToString("P");
            Label6.Text = P6.ToString("P");
            Label7.Text = P7.ToString("P");
            Label8.Text = P8.ToString("P");
            Label9.Text = P9.ToString("P");
            Label10.Text = P10.ToString("P");

            //Label23.Text = P1.ToString();
            Label24.Text = P2.ToString();
            Label25.Text = P3.ToString();
            Label26.Text = P4.ToString();
            Label27.Text = P5.ToString();
            Label28.Text = P6.ToString();
            Label29.Text = P7.ToString();
            Label30.Text = P8.ToString();
            Label31.Text = P9.ToString();
            Label32.Text = P10.ToString();

            CheckBoxList1.Visible = false;

            DropDownList1.Text = Winner.Name;
        }





        protected void btnEditAll_Click(object sender, EventArgs e)
        {
            btnEditAll.Visible = false;
            btnConfirmAll.Visible = true;
            btnConfirmPlanes.Visible = false;
            

            TextBox1.Visible = true;
            TextBox2.Visible = true;
            TextBox3.Visible = true;
            TextBox4.Visible = true;
            TextBox5.Visible = true;
            TextBox6.Visible = true;
            TextBox7.Visible = true;
            TextBox8.Visible = true;
            TextBox9.Visible = true;
            TextBox10.Visible = true;

        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBoxList1.Visible = true;
            DropDownList2.Visible = false;
        }

        protected void btnConfirmAll_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TextBox1.Text))
            {
                var P1 = MiniBoeing.PAX1 = Convert.ToDouble(TextBox1.Text)/100;
                Label1.Text = P1.ToString();
            }
            if (!String.IsNullOrEmpty(TextBox2.Text))
            {
                var two = MiniBoeing.PAX2 = Convert.ToDouble(TextBox2.Text) / 100;
                Label2.Text = two.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox3.Text))
            {
                var three = MiniBoeing.PAX3 = Convert.ToDouble(TextBox3.Text) / 100;
                Label3.Text = three.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox4.Text))
            {
                var four = MiniBoeing.PAX4 = Convert.ToDouble(TextBox4.Text) / 100;
                Label4.Text = four.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox5.Text))
            {
                var P1 = MiniBoeing.PAX5 = Convert.ToDouble(TextBox5.Text);
                Label5.Text = P1.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox6.Text))
            {
                var P1 = MiniBoeing.PAX6 = Convert.ToDouble(TextBox6.Text);
                Label6.Text = P1.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox7.Text))
            {
                var P1 = MiniBoeing.PAX7 = Convert.ToDouble(TextBox7.Text);
                Label7.Text = P1.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox8.Text))
            {
                var P1 = MiniBoeing.PAX8 = Convert.ToDouble(TextBox8.Text);
                Label8.Text = P1.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox9.Text))
            {
                var P1 = MiniBoeing.PAX9 = Convert.ToDouble(TextBox9.Text);
                Label9.Text = P1.ToString("P");
            }
            if (!String.IsNullOrEmpty(TextBox10.Text))
            {
                var P1 = MiniBoeing.PAX10 = Convert.ToDouble(TextBox10.Text);
                Label10.Text = P1.ToString("P");
            }

            TextBox1.Visible = false;
            TextBox2.Visible = false;
            TextBox3.Visible = false;
            TextBox4.Visible = false;
            TextBox5.Visible = false;
            TextBox6.Visible = false;
            TextBox7.Visible = false;
            TextBox8.Visible = false;
            TextBox9.Visible = false;
            TextBox10.Visible = false;

            var start = MiniBoeing.PAX1 + 1;
            Label13.Text = 1.ToString();
            Label14.Text = (start + MiniBoeing.PAX2).ToString();
            Label15.Text = (MiniBoeing.PAX2 + MiniBoeing.PAX3).ToString();
            Label16.Text = (MiniBoeing.PAX3 + MiniBoeing.PAX4).ToString();
            Label17.Text = (MiniBoeing.PAX4 + MiniBoeing.PAX5).ToString();
            Label18.Text = MiniBoeing.PAX6.ToString();
            Label19.Text = MiniBoeing.PAX7.ToString();
            Label20.Text = MiniBoeing.PAX8.ToString();
            Label21.Text = MiniBoeing.PAX9.ToString();
            Label22.Text = MiniBoeing.PAX10.ToString();
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var P1 = MiniBoeing.PAX1 = .05;
            var P2 = MiniBoeing.PAX2 = .05;
            var P3 = MiniBoeing.PAX3 = .03;
            var P4 = MiniBoeing.PAX4 = .03;
            var P5 = MiniBoeing.PAX5 = .025;
            var P6 = MiniBoeing.PAX6 = .025;
            var P7 = MiniBoeing.PAX7 = .02;
            var P8 = MiniBoeing.PAX8 = .02;
            var P9 = MiniBoeing.PAX9 = .01;
            var P10 = MiniBoeing.PAX10 = .01;

            //Label23.Text = P1.ToString();
            Label24.Text = P2.ToString();
            Label25.Text = P3.ToString();
            Label26.Text = P4.ToString();
            Label27.Text = P5.ToString();
            Label28.Text = P6.ToString();
            Label29.Text = P7.ToString();
            Label30.Text = P8.ToString();
            Label31.Text = P9.ToString();
            Label32.Text = P10.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}