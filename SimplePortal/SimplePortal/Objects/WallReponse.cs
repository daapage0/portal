﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplePortal.Objects
{
//    {
//          status: success || error(or this can be success : True || False),
//          errorMessage: error message, if the operation failed(wall or function),
//          result: the returned value from the function
//      }

public class WallResponse<T1>
    {
        ////response parameters on 12.11 were slightly different
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error")]
        public string Error {get; set;}

        [JsonProperty("data")]
        public T1 Data { get; set; }

        //[JsonProperty("errorDetails")]
        //public string ErrorDetails { get; set; }

        //[JsonProperty("error")]
        //public string Error {get; set;}
    }
}