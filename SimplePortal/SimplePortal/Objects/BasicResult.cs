﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplePortal.Objects
{
    public class OperationResult
    {
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
    }
}