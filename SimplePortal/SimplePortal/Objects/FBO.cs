﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//goal = make general FBO class to instantiate FBOs when pulling info from Mongo
//the fields in FBO should directly match the mongo collection for FBO

namespace SimplePortal.Objects
{

    public class FboResponse
    {
        //public JsonArrayAttribute fbo { get; set; }
        //public List<FBO> fbo { get; set; }
        // public FBO fbo { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("errorMsg")]
        public string ErrorMsg { get; set; }
        [JsonProperty("errorDetails")]
        public string ErrorDetails { get; set; }
 
        [JsonProperty("fbo")]
        public FBO Fbo { get; set; }
        //public FBO Fbo { get; set; }
    }
    public class FBO
    {
            //public string name { get; set; }
            //public double percentIncrease { get; set; }
            //public string phoneNumber { get; set; }
            //public double costPerHour { get; set; }
            //public int dailyCharge { get; set; }
            //public string agreement { get; set; }
            //public int increaseFrequency { get; set; }
            //public string id { get; set; }
            //public string email { get; set; }
            //public int flatCharge { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("percentIncrease")]
        public string PercentIncrease { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("flatCharge")]
        public double FlatCharge { get; set; }

        [JsonProperty("costPerHour")]
        public double CostPerHour { get; set; }

        [JsonProperty("dailyCharge")]
        public double DailyCharge { get; set; }

        [JsonProperty("agreement")]
        public string Agreement { get; set; }

        [JsonProperty("increaseFrequency")]
        public string IncreaseFrequency { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }



        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("errorMsg")]
        public string ErrorMsg { get; set; }
        [JsonProperty("errorDetails")]
        public string ErrorDetails { get; set; }
    }

}