﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplePortal.Objects
{
    public class PAXFactors
    {
        public string Aircraft { get; set; }
        public double PAX1 { get; set; }
        public double PAX2 { get; set; }
        public double PAX3 { get; set; }
        public double PAX4 { get; set; }
        public double PAX5 { get; set; }
        public double PAX6 { get; set; }
        public double PAX7 { get; set; }
        public double PAX8 { get; set; }
        public double PAX9 { get; set; }
        public double PAX10 { get; set; }

        public PAXFactors(string aircraft)
        {
            Aircraft = aircraft;
        }
    }
}