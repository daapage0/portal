﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplePortal.Objects
{
    /// <summary>
    /// this is the secure wall request
    /// </summary>
    public class WallRequestSecure
    {
        public string function { get; set; }
        public string hash { get; set; }
        public object functionParams { get; set; }
        public string salt { get; set; }
        public string sentTime { get; set; }
        public string username { get; set; }
    }
}